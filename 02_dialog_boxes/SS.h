#ifndef SS_H
#define SS_H

#include<QApplication>
#include<QWidget>
#include<QPushButton>
#include<QMessageBox>
#include<QInputDialog>

class SS : public QWidget {
    Q_OBJECT
    public:
        SS();
    private:
        QPushButton *Btn1;
        QPushButton *Btn2;
    public slots:
        void Message();
        void GetInfo();
};

#endif //SS_H
