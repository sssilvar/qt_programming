#include<QApplication>
#include<QMessageBox>

#include"SS.h"

main(int argc, char *argv[])
{
    // Define App and elements
    QApplication app(argc, argv);
    SS ss;

    // Show and execute App
    ss.show();
    return app.exec();
}
