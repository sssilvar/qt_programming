#include"SS.h"

SS::SS():QWidget()
{
    // Create elements
    Btn1 = new QPushButton("Information", this);
    Btn2 = new QPushButton("Get Info From User", this);

    // Set properties
    setFixedSize(300, 90);
    Btn2 -> move(100, 0);

    // Connect slots
    connect(Btn1, SIGNAL(clicked()), this, SLOT(Message()));
    connect(Btn2, SIGNAL(clicked()), this, SLOT(GetInfo()));
}

// Define Slots
void SS::Message() {
    // QMessageBox::information(this, "Information", "Your downloading is finished!", "OK");
    int respond = QMessageBox::question(
        this, 
        "Save file", 
        "Do you want to save the file?", 
        QMessageBox::Yes|QMessageBox::No);

    // React to respond
    if (respond == QMessageBox::Yes) {
        QMessageBox::information(this, "Saving file", "Your File has been saved.");
    }
}

void SS::GetInfo() {
    bool ok;
    QString passwd = QInputDialog::getText(
        this, 
        "Password", 
        "Insert your password", 
        QLineEdit::Password,
        QString(),
        &ok);
    
    if (ok && !passwd.isEmpty()) {
        QMessageBox::information(this, "Information", "Your password has been saved!");
    } else if (ok && passwd.isEmpty()) {
        QMessageBox::critical(this, "Error", "you must insert your password");
    }
    
}