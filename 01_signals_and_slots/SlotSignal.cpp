#include"SlotSignal.h"

SlotSignal::SlotSignal():QWidget()  
{
    // setFixedSize(300, 90);
    Btn1 = new QPushButton("Reveal", this);
    Btn1 -> move(60, 70);
    
    label = new QLabel("Here is your text!", this);
    label -> setVisible(false);

    // Create connection
    // connect(Btn1, SIGNAL(clicked()), qApp, SLOT(quit()));  // Exit the App
    connect(Btn1, SIGNAL(clicked()), this, SLOT(reveal()));
    connect(this, SIGNAL(invLabel()), this, SLOT(showFullScreen()));
}

/*
* Implement reveal()
*/
void SlotSignal::reveal() {
    bool isVisible = !label->isVisible();
    QString st = isVisible? "True" : "False";
    qDebug(st.toLatin1());
    label -> setVisible(isVisible);

    if (isVisible) {
        emit invLabel();
    }
}