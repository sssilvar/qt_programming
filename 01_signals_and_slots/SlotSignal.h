#ifndef SlotSignal_H
#define SlotSignal_H

#include<QApplication>
#include<QWidget>
#include<QPushButton>
#include<QLabel>

class SlotSignal: public QWidget
{
    Q_OBJECT
public:
    SlotSignal();

private:
    QPushButton *Btn1;
    QLabel *label;

public slots:
    void reveal();

signals:
    void invLabel();
};

#endif // SlotSignal_H