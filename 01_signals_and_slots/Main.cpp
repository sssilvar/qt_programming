#include<QApplication>
#include<QPushButton>
#include<QProgressBar>
#include<QFont>

#include"SlotSignal.h"

main(int argc, char *argv[]) {
    // Create App and elements
    QApplication app(argc, argv);
    SlotSignal Ss;

    // Show elwments
    Ss.show();
    return app.exec();
}
