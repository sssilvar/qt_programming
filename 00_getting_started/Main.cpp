#include<QApplication>
#include<QPushButton>
#include<QProgressBar>
#include<QFont>

main(int argc, char *argv[])
{
    // Create Elements (Objects)
    QApplication app(argc, argv);
    QWidget window;
    QPushButton Btn("OK", &window);
    QPushButton Btn2("Cancel", &window);
    QProgressBar ProgressBar(&window);

    // Set fonts
    QFont MyFont("Carlito", -1, QFont::Normal, false);

    // Set properties
    window.setFixedSize(300, 150);

    Btn.setToolTip("You agree with this");
    Btn.setFont(MyFont);
    Btn.move(50,70);

    Btn2.setToolTip("You are about to leave the application");
    Btn2.setFont(MyFont);
    Btn2.setCursor(Qt::PointingHandCursor);
    Btn2.move(150,70);

    ProgressBar.setValue(70);

    // Visualize elements and execute the App
    window.show();
    return app.exec();
}
